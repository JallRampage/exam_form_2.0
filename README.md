# Sprint One

## Actividades:

- Usando Laravel Breeze con autenticación y registro, el sistema debe contener lo siguiente:
    - **[ ACTIVIDAD 1 ]** Usando Seeders llenar de materias, grupos y alumnos para poder asignarles exámenes a contestar. Y un administrador para dar de alta los exámenes y preguntas y respuestas del mismo. (También se pueden agregar algunos exámenes desde el Seeder, pero no todos, ya que está es una función del administrador). Esto es fundamental que se haga al inicio del desarrollo y en esta parte es en donde se hacen las relaciones entre las tablas de la base de datos.**[URGENTE]**
        - Las tablas que conformaran el sprint son:
            - Users.
            - Students
            - Admins
            - subjects.
            - SchoolGroups.
            - TODO: Revisar que tablas faltan y sus relaciones pensando en que hay preguntas, respuestas y examenes.
            
            Recordar que todos los estudiantes son usuarios, pero no todos los usuarios son estudiantes, por eso es que hay tres tablas que hacen referencia a usuarios y además esto se hace por temas de segerida en la normalización de tablas. Igual creo que es importante que lo comentemos y todos estemos de acuerdo, puedo estarme equivocando.
            
    - **[ MODULO 1 ]** El administrador podrá dar de alta los exámenes, en donde configura las preguntas y respuestas de opción múltiple (incluyendo la respuesta correcta). El administrador NO da de alta las materias y los grupos, esto se hace usando seeders
        - Una materia puede tener múltiples preguntas, pero solo toma 10 preguntas al azar para el examen y las debe mostrar siempre en desorden. Y se pueden agregar en cualquier momento más preguntas, editar y desactivar. (Logica de la pantalla del estudiante)
    - Los exámenes se califican de 0 a 100, donde cada respuesta correcta equivale a 10 puntos. Solo se puede contestar una vez y en el momento de empezar (si se sale, las respuestas se toman como no contestadas).(Logica de la pantalla del estudiante)
    - Las calificaciones de los alumnos se guardarán en la tabla de grupos. Un alumno puede pertenecer a varios grupos y contestar más de un examen (si es que se encuentra configurado). Y el alumno podrá ver desde su cuenta las calificaciones obtenidas. (Logica de la pantalla del estudiante y relacion de base de datos)
    - **[ MODULO 2 ]** El administrador podrá descargar visualizar las calificaciones por grupo y descargar un CSV con esos datos. Este es un submódulo dentro de la pantalla del usuario tipo administrador
        
        **Modulos y actividades no escritas por el Profe, pero si visibles:**
        
    - **[ MODULO 3 ]** Pantalla del usuario tipo estudiante, en la que podrá consultar los exámenes que tiene activos. En esta pantalla se mostrará el listado de las materias disponibles, y cada materia tendrá un botón que dice “Contestar examen”. Este botón solo estará activado y será posible dar click si es que el usuario tiene un examen asignado.
    - **[ MODULO 4 ]** Si bien se utilizará breeze para la autenticación, hay que darle diseño enfocado al tema de desarrollo a la pantalla de inicio de sesión y registro. Preguntar al profesor: Además se debe contemplar que si bien, a través de seeders se añadirán los grupos que a su vez están relacionados con las materias, algunos usuarios se deben de registrar y en el formulario de registro (Echo con brezze), se debe de añadir la opción de seleccionar el grupo al que el estudiante pertenecerá.
    - **[ ACTIVIDAD 2 ]** Algo que puede facilitar el desarrollo es que cada quien estile con taildwind conforme esta trabajando y UNA PERSONA se dedique a hacer componentes reutilizables para luego unificar la apariencia de todos los módulos de la aplicación de manera rapida y sencilla AL FINAL DEL DESARROLLO, ya que al darnos la libertad de que cada quien estile su modulo a desarrollar, al final los módulos se verán diferentes y parecerá que no ubo comunicación y ni siquiera nos pudimos poner de acuerdo en que colores usar. **[PRIORITARIA]**
    - **[ ACTIVIDAD 3 ]** Inicializar el proyecto y llevar el manejo del git flow.
- Debe llevar modelos correctamente creados, migraciones, seeders, controllers y rutas. Todas las query se hacen con Eloquent. Por parte del Front se tiene que usar clases de Tailwind y no debe llevar CSS ni estilos personalizados, pero si puede llevar JS.

En resumen tenemos:

- Modulo 1: Modulo de administración. **[JAFET]**
- Modulo 2: Modulo de reportes. **[ALAN]**
- Modulo 3: Modulo del estudiante. [**ANDREA]**
- Modulo 4: Modulo de inicio de sesión. **[JOSE]**
- Actividad 1: Seeders y arquitectura de la base de datos **[URGENTE]**. **[JOSE]**
- Actividad 2: Unificación de estilos **[PRIORITARIA]**. **[ALAN]**
- Actividad 3: Manejo de conflictos.**[PRIMORDIAL]** **[JOSE]**