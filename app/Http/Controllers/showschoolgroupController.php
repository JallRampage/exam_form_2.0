<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class showschoolgroupController extends Controller
{
    public $aux = 1;
    
    public function index()
    {
        
        /*$alumno = Alumno::leftJoin(
            'carreras', 'carreras.carrera_id', 'alumnos.carrera_id'
        )->where('user_id', auth()->user()->id)->first();*/
            
        return view('schoolgroups',['aux' => $this->aux]);
    }
    public function aux()
    {            
        return view('subjects_student');
    }
    public function grade()
    {            
        return view('grades');
    }
}

