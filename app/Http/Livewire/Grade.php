<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\Exam;
use App\Models\Student;
use App\Models\SchoolGroup;
use App\Models\Evaluation;
use App\Models\Subject;

class Grade extends Component
{
    public $id_group;
    public $exam_search_aux;
    public $aux3=1;
    public $exams;
    public function render()
    {
        $student_id=Student::Where('user_id',auth()->user()->id)->first();
        $school_group_info=SchoolGroup::Where('student_id',$student_id->student_id)->get();
        
        $subject = collect();
        $this->id_group=collect();
         foreach ($school_group_info as $u) {
             $subject->push(Subject::where('subject_id', $u->subject_id)->first());
             $this->id_group->push($u->id);
         }
        return view('livewire.grade',[
            'school_group_info'=>$school_group_info,
        ]);
    }
    public function gotoexams($subject_id)
    {
        $student_id=Student::Where('user_id',auth()->user()->id)->first();
        
        $this->aux3++;
        $this->exam_search_aux=$subject_id;
        $this->exams = Evaluation::where('subject_id',$this->exam_search_aux)->Where('student_id',$student_id->student_id)->get();

    }
}
