<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Question;
use App\Models\Exam;
use App\Models\Answer;


class QuestionConfig extends Component
{
    public $busqueda;
    public $questions;
    public $prueba;
    public $exam_id;
    public $questions_modificadas;
    protected $rules = [
        'questions_modificadas.*.question_content' => 'string',
        'questions_modificadas.*.answers.*.answer_content' => 'string',
        'questions_modificadas.*.answers.*.is_correct' => 'integer',
        // ...
    ];
    protected $listeners = ['refreshComponent' => '$refresh'];
    public function render()
    {
        $exams = Exam::where('exam_id',$this->exam_id)->get();
        $this->questions = Question::with('answers')->where('exam_id', $this->exam_id)->get();
        
        if ($this->questions_modificadas !== null && !$this->questions_modificadas->isEmpty()) {
            $this->questions = $this->questions_modificadas;
        }else{
            $this->questions_modificadas=$this->questions;
        }
        if (!$exams->isEmpty()) {
            $exam_name = $exams->first()->exam_name;
        } else {
            $exam_name=null;
        }
        
        return view('livewire.question-config', [
            'exams'=>$exams,
            'exam_name'=>$exam_name,
            'questions'=>$this->questions
            
    
        ]);

    }



    public function guardarCambios()
    {
        $aux1 = collect();
        foreach ($this->questions_modificadas as $index => $preguntas) {
            // Actualizar el contenido de la pregunta en la propiedad $questions_modificadas
            $this->questions_modificadas[$index]['question_content'] = $preguntas['question_content'];
           
        
            // Guardar la pregunta modificada en la base de datos
            Question::where('question_id', $preguntas['question_id'])
                ->update(['question_content' => $this->questions_modificadas[$index]['question_content']]);
            foreach ($preguntas['answers'] as $answer_index => $answer) {
   
        
    $aux1->push($this->questions_modificadas[$index ]['answers'][ $answer_index]['is_correct']);
        
   if($this->questions_modificadas[$index ]['answers'][ $answer_index]['is_correct']==="1"){
    $aux2=Answer::where('question_id', $preguntas['question_id'])->get();
    for ($i = 0; $i < $aux2->count(); $i++) {
        if($aux2[$i]->is_correct==1){
           $aux2[$i]->is_correct=0;
           $aux2[$i]->save(); 
        }
      }
    $answer_model = Answer::find($answer['answer_id']);
    $answer_model->is_correct = 1;
    $answer_model->save();
   }
    Answer::where('answer_id', $answer['answer_id'])
        ->update([
            'answer_content' => $answer['answer_content']
            
        ]);    
    }
        }
        $this->questions = $this->questions_modificadas;
      
        // Mensaje de éxito
        session()->flash('message', 'Cambios guardados con éxito.');
        $this->emit('refreshComponent');
    }
    
    
   
}
