<?php

namespace App\Http\Livewire;
use App\Models\Subject;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Evaluation;
use Livewire\Component;
use Livewire\WithPagination;

class ShowSchoolgroups extends Component
{
    use WithPagination;

    public $paginas = 10;
    public $busqueda;
    public $aux=1;
    public $newexman_modal = false;
    public $title = 'New exam';
    public $ex_name;
    public $id_sub;
    public $id_ex;
    public $qm;
    protected $rules = [
        'qm.*.question_content' => 'string','required',
        'qm.*.answers.*.answer_content' => 'string',
        'qm.*.answers.*.is_correct' => 'integer',
        'ex_name' => 'string','required',
        // ...
    ];

public function render()
{
    $SchoolGroups = Subject::orderBy('subject_name', 'desc')->paginate($this->paginas);
    $exams = Exam::where('subject_id',$this->busqueda)->get();
    $questions = Question::with('answers')->where('exam_id', $this->busqueda)->get();
    if (!$exams->isEmpty()) {
        $exam_name = $exams->first()->exam_name;
    } else {
        $exam_name=null;
    }
    return view('livewire.show-schoolgroups', [
        'SchoolGroups' => $SchoolGroups,
        'aux' => $this->aux,
        'exams'=>$exams,
        'exam_name'=>$exam_name,
        'questions'=>$questions
        

    ]);
   
    
    

}
public function gotoexams($subject_id)
{
    $this->aux++;
    $this->busqueda=$subject_id;
    $this->id_sub=$subject_id;
    
}
public function gotoquestions($examid)
{
    $this->aux++;
    $this->id_ex=$examid;
    
    
    
}
public function changestatus($examid)
{
   $exam_change= Exam::where('exam_id',$examid)->first();
    if($exam_change->status==0){
        $exam_change->status=1;
        $exam_change->save();
    }else{
        $restart=Evaluation::Where('exam_id',$examid)->get();
        foreach ($restart as $index => $i) {
            $restart[$index]->answered=0;
            $restart[$index]->save();
        }
        $exam_change->status=0;
        $exam_change->save();
    }
    
}
public function prepare()
{
    $this->title = 'New Exam';
    $this->newexman_modal = true;
}
public function create_exam($subject_id){
    
    $exam_new=Exam::create([
        'subject_id' => $subject_id,
        'exam_name' => $this->ex_name,
        // Agrega aquí todos los campos y valores que desees guardar
    ]);
    $this->savequestion($exam_new->exam_id);
    $this->newexman_modal = false;
}
public function savequestion($exam_idk){
    $aux=collect();
    foreach ($this->qm as $index => $question){
        $question_c=$this->qm[$index]['question_content'];
        $question_new=Question::create([
            'exam_id' => $exam_idk,
            'question_content' => $question_c,
            
        ]);
        foreach ($question['answers'] as $index_answer => $answera){ 
        $this->saveanswer($question_new->question_id,$this->qm[$index]['answers'][$index_answer]['is_correct'],$this->qm[$index]['answers'][$index_answer]['answer_content']);

        }
       
    }
    
}
//funcion para verificar que no haya respestas correctas guardas anteriormente
public function comprobar($i,$j){
    for($h=1;$h<=4;$h++){
        if(isset($this->qm[$i]['answers'][$h]) && array_key_exists('is_correct',$this->qm[$i]['answers'][$h]) && $this->qm[$i]['answers'][$h]['is_correct']=1){
            $this->qm[$i]['answers'][$h]['is_correct']=0;
        }else{
            $this->qm[$i]['answers'][$h]['is_correct']=0;
        }
    }
    $this->qm[$i]['answers'][$j]['is_correct']=1;
}
public function saveanswer($question_id,$is_correct,$answer_cont){


    Answer::create([
        'question_id' => $question_id,
        'answer_content' => $answer_cont,
        'is_correct' => $is_correct,
        
    ]);
    
    
}




}
