<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use App\Models\Exam;
use App\Models\Question;
use App\Models\Subject;
use App\Models\Student;
use App\Models\SchoolGroup;
use App\Models\Evaluation;

class ShowSubjects extends Component
{
    public $aux2=1;
    public $aux_cont=0;
    public $do;
    public $exam_search;
    public $questions;
    public $questionsaux;
    public $exam_name;
    public $answers_student;
    public $group_name;
    public $id_group_collect;
    public $school_group_id;
    public $evaluation;
    protected $rules = [
        'questions.*.question_content' => 'string','required',
        'questions.*.answers.*.answer_content' => 'string','required',
        'questions.*.answers.*.is_correct' => 'integer','required',
        'answers_student.*.answer.*.is_correct' => 'integer','required',
    ];
    protected $listeners = ['refreshComponent' => '$refresh'];
    public function render()
    {
        $student_id=Student::Where('user_id',auth()->user()->id)->first();
        $school_group_info=SchoolGroup::Where('student_id',$student_id->student_id)->get();
        $exams = Exam::where('subject_id',$this->exam_search)->get();
        
        $subject = collect();
       $this->id_group_collect=collect();

        foreach ($school_group_info as $u) {
            $subject->push(Subject::where('subject_id', $u->subject_id)->first());
            $this->id_group_collect->push($u->id);
        }
        return view('livewire.show-subjects',[
            'subject'=>$subject,
            'exams'=>$exams,
            'aux2'=>$this->aux2,
        ]);
    }
    public function gotoexams($subject_id,$group_id)
    {
        $student_id=Student::Where('user_id',auth()->user()->id)->first();
        $this->aux2++;
        $this->exam_search=$subject_id;
        $this->id_sub=$subject_id;
        $this->school_group_id=$group_id;
        $this->evaluation = Evaluation::where('student_id', $student_id->student_id)->Where('subject_id',$this->exam_search)->get();
        $this->aux_cont=count($this->evaluation)-1;
        
        if ($this->evaluation->count() == 0) {
            $this->do=false;
        } else {
            $this->do=true;
        }    
    }
    public function gototest($exam_id){
        $this->aux2++;
        $this->exam_name=Exam::Where('exam_id',$exam_id)->first();
        $this->questions = Question::with('answers')->where('exam_id', $exam_id)->inRandomOrder()->take(10)->get();
        //dd($this->questions[1]->question_content);
    }
    public function calificar(){
        $aux1 = collect();
        $grade=0;
        foreach ($this->questions as $index => $preguntas) {
            foreach ($preguntas['answers'] as $answer_index => $answer) {
                if(isset($this->answers_student[$index ]['answer'][ $answer_index]['is_correct'])){
                    if($this->answers_student[$index ]['answer'][ $answer_index]['is_correct']==$this->questions[$index ]['answers'][ $answer_index]['is_correct'])
                    {
                        $grade+=10;
                    }    
                }
            }
        }
        //asignar el score
        //$n_exam=Exam::Where('subject_id',$this->exam_search)->count();
       /* $update_grade=SchoolGroup::find($this->school_group_id);
        $update_grade->score=$grade;
        $update_grade->save();
        */
        
        //crear registro
        $student_id=Student::Where('user_id',auth()->user()->id)->first();
        //comprobar si ya hay registro
        $buscar=Evaluation::Where('exam_id',$this->exam_name->exam_id)->
        Where('student_id',$student_id->student_id)->
        Where('subject_id',$this->exam_search)->exists();
        $buscar_cambiar=Evaluation::Where('exam_id',$this->exam_name->exam_id)->
        Where('student_id',$student_id->student_id)->
        Where('subject_id',$this->exam_search)->first();
        if($buscar){
            $buscar_cambiar->grade=$grade;
            $buscar_cambiar->answered=1;
            $buscar_cambiar->save();
        }else{

        $update_answered=Evaluation::Create(['exam_id'=>$this->exam_name->exam_id,
                                            'student_id'=>$student_id->student_id,
                                            'subject_id'=>$this->exam_search,
                                            'grade'=>$grade,
                                            'answered'=>1]);
                                        }
        session()->flash('message', 'Se ha enviado correctamente el examen');
        return redirect()->route('subjects_student');
        
    }
}
