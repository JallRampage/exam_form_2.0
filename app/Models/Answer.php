<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    protected $table = "answers";
    protected $primaryKey = 'answer_id';
    public $incrementing = true;

    protected $fillable = [
        'question_id',
        'answer_content',
        'is_correct'
    ];

    //Retations with Question Model
    public function question()
    {
        return $this->belongsTo(Question::class, 'question_id', 'question_id');
    }
}
