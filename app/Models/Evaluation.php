<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use HasFactory;
    protected $table = "evaluations";
    protected $primaryKey = 'evaluations_id';
    public $incrementing = true;
    protected $fillable = [
        'exam_id',
        'student_id',
        'subject_id',
        'grade',
        'answered'
    ];
    public function exam()
    {
        return $this->belongsTo(Exam::class, 'exam_id', 'exam_id');
    }
}
