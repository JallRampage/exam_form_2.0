<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;
    
    protected $table = "exams";
    protected $primaryKey = 'exam_id';
    public $incrementing = true;

    protected $fillable = [
        'subject_id',
        'exam_name',
        'status'
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'subject_id');
    }


}
