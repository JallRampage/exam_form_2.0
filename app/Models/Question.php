<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $table = "questions";
    protected $primaryKey = 'question_id';
    public $incrementing = true;

    protected $fillable = [
        'question_content',
        'exam_id'
    ];

    //Retations with Exam Model
    public function exam()
    {
        return $this->belongsTo(Exam::class, 'exam_id', 'exam_id');
    }
    public function answers()
{
    return $this->hasMany(Answer::class, 'question_id', 'question_id');
}


    
}
