<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SchoolGroup extends Model
{
    use HasFactory;
    protected $table = "school_groups";
    protected $primaryKey = 'id';
    public $incrementing = true;

    protected $fillable = [
        'subject_id',
        'student_id',
        'group_name',
        'score'
    ];

    //Retations with Question Model
    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'subject_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'student_id');
    }
}
