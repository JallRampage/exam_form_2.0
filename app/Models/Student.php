<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    
    protected $table = "students";
    protected $primaryKey = 'student_id';
    public $incrementing = true;

    protected $fillable = [
        'user_id',
        'control_number', 
        'semester',
    ];

    //TODO: MAKE RELATION WITH ANOTHER TABLES
    /*public function carrera()
    {
        return $this->belongsTo(Carrera::class, 'carrera_id', 'carrera_id');
    }*/

}
