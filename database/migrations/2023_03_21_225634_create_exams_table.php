<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('exams', function (Blueprint $table) {
            //Automatic fill
            $table->id('exam_id');

            //Colums
            $table->unsignedBigInteger('subject_id');
            $table->string('exam_name', 100);
            $table->boolean('status')->default(false);

            //References
            $table->timestamps();

            //Foreing keys
            $table->foreign('subject_id')->references('subject_id')->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('exams');
    }
};
