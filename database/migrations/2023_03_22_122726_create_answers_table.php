<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('answers', function (Blueprint $table) {
            //Automatic fill
            $table->id('answer_id');

            //Colums
            $table->unsignedBigInteger('question_id');
            $table->string('answer_content', 255);
            $table->boolean('is_correct');

            //References
            $table->timestamps();

            //Foreing keys
            $table->foreign('question_id')->references('question_id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('answers');
    }
};
