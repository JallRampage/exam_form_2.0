<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('school_groups', function (Blueprint $table) {
            //Automatic fill
            $table->id();

            //Colums
            $table->unsignedBigInteger('subject_id');
            $table->unsignedBigInteger('student_id');
            $table->string('group_name', 255);
            $table->float('score',8,2)->nullable();

            //References
            $table->timestamps();

            //Foreing keys
            $table->foreign('subject_id')->references('subject_id')->on('subjects');
            $table->foreign('student_id')->references('student_id')->on('students');
        
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('school_groups');
    }
};
