<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{

    public function run(): void
    {
        $first_user_id = 1;
        $password_suffix = 'Password#$';

        for ($i = 0; $i < 1; $i++) {
            $name = $this->generateRandomName();
            $email = $this->generateEmail($name);
            $password = bcrypt($this->getFirstLastName($name) . $password_suffix);

            User::create([
                'name' => $name,
                'email' => $email,
                'password' => $password,
            ])->assignRole('Admin');

            Admin::create([
                'user_id' => $first_user_id + $i,
            ]);

        }
    }

    /**
     * Generates a random name with two surnames
     * @return string
     */
    private function generateRandomName(): string
    {
        $name = '';
        $first_names = ['Emma', 'Liam', 'Olivia', 'Noah', 'Ava', 'Ethan', 'Sophia', 'Logan', 'Mia', 'Lucas', 'Amelia', 'Mason', 'Harper', 'Elijah', 'Evelyn', 'Oliver', 'Abigail', 'Jacob', 'Emily', 'William', 'Elizabeth', 'Michael', 'Charlotte', 'Alexander', 'Avery', 'James', 'Sofia', 'Benjamin', 'Chloe', 'Carter', 'Grace', 'Luke', 'Aria', 'Daniel', 'Hazel', 'Matthew', 'Luna', 'Ryan', 'Ella', 'Henry', 'Ellie', 'Emily', 'Aiden', 'Leah', 'Jackson', 'Lily', 'Madison', 'Eva'];
        $last_names = ['Smith', 'Johnson', 'Brown', 'Lee', 'Garcia', 'Rodriguez', 'Martinez', 'Davis', 'Hernandez', 'Lopez', 'Gonzalez', 'Perez', 'Moore', 'Miller', 'Jackson', 'Martin', 'White', 'Clark', 'Lewis', 'Hall', 'Allen', 'Young', 'King', 'Wright', 'Scott', 'Green', 'Baker', 'Nelson', 'Carter', 'Mitchell', 'Perez', 'Roberts', 'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Collins', 'Stewart', 'Sanchez', 'Morris', 'Rogers', 'Reed', 'Cook', 'Morgan', 'Cooper', 'Murphy', 'Peterson', 'Bailey', 'Rivera'];
        $name .= $first_names[array_rand($first_names)] . ' ';
        $name .= $last_names[array_rand($last_names)] . ' ';
        $name .= $last_names[array_rand($last_names)];
        return $name;
    }

    /**
     * Generates an email for a given name
     * @param string $name
     * @return string
     */
    private function generateEmail(string $name): string
    {
        $name_parts = explode(' ', $name);
        $base_email = strtolower($name_parts[0] . '.' . $name_parts[1]);
        $email = $base_email . '@morelia.tecnm.mx';

        // Check if the email already exists in the database
        $existing_user = User::where('email', $email)->first();
        if ($existing_user) {
            // Append a random number to the base email until it is unique
            $i = 1;
            do {
                $email = $base_email . $i . '@morelia.tecnm.mx';
                $existing_user = User::where('email', $email)->first();
                $i++;
            } while ($existing_user);
        }

        return $email;
    }

    /**
     * Extracts the first last name from a full name
     * @param string $name
     * @return string
     */
    private function getFirstLastName(string $name): string
    {
        $name_parts = explode(' ', $name);
        return $name_parts[1];
    }

    
}
