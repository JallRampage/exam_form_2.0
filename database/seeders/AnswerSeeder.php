<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Exam;
use App\Models\Question;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AnswerSeeder extends Seeder
{
    public function run(): void
    {

        $faker = Faker::create();
        $exams = Exam::all();

        foreach ($exams as $exam) {
            $questions = Question::where('exam_id', $exam->exam_id)->get();

            foreach ($questions as $question) {
                $correctAnswerIndex = rand(0, 3);

                for ($i = 0; $i < 4; $i++) {
                    $answer_content = $faker->sentence($nbWords = 6, $variableNbWords = true);

                    Answer::create([
                        'question_id' => $question->question_id,
                        'answer_content' => $answer_content,
                        'is_correct' => $i === $correctAnswerIndex,
                    ]);
                }
            }
        }
    }
}
