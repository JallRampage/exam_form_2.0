<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\Subject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $subjects = Subject::all();
        $faker = Faker::create();

        // Create 80 random exams
        for ($i = 1; $i <= 5; $i++) {

            
            Exam::create([
                    'subject_id' => $i,
                    'exam_name' => $faker->randomElement([
                    'Mathematics', 'Biology', 'Chemistry', 'Physics', 'History', 
                    'Geography', 'English', 'Spanish', 'Computer Science', 'Art'
                ])
            ]);
        }
    }
}
