<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\Question;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $numQuestionsPerExam = 20;
        
        // Get the number of exams using Eloquent
        $numExams = Exam::count();
        
        // Generate questions for each exam
        for ($i = 1; $i <= $numExams; $i++) {
            for ($j = 1; $j <= $numQuestionsPerExam; $j++) {
                // Get a random exam using Eloquent
                $exam = Exam::inRandomOrder()->first();
                
                // Generate a unique question content using Faker
                $faker = \Faker\Factory::create();
                $unique = false;
                while (!$unique) {
                    $questionContent = $faker->sentence(10);
                    $unique = !Question::where('exam_id', $exam->exam_id)
                        ->where('question_content', $questionContent)
                        ->exists();
                }
                
                // Create the question
                Question::create([
                    'exam_id' => $exam->exam_id,
                    'question_content' => $questionContent,
                ]);
            }
        }
    }
}
