<?php

namespace Database\Seeders;

use App\Models\Exam;
use App\Models\SchoolGroup;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolGroupSeeder extends Seeder
{
    public function run(): void
    {
        $students = Student::all();
        $subjects = Subject::all();
        
        $groupNames = ['a', 'b'];
        $groupNameIndex = 0;
        $groupCounter = 0;

        foreach ($students as $student) {
            if($student->student_id<=15){
                for ($i = 0; $i <=2; $i++) {
                    // Choose the subject for the current iteration
                    

                    $subject_id = null;
                    $exists = true;
                    while($exists) {
                        $subject_id = random_int(1, 5);
                        $exists = SchoolGroup::where('subject_id', $subject_id)->Where('student_id',$student->student_id)->exists();
                    }
                    
                    SchoolGroup::create([
                        'student_id' => $student->student_id,
                        'subject_id' => $subject_id,
                        'group_name' => 'a',
                        'score' => 0.00,
                    ]);
                    
                }
            }else{
                for ($i = 0; $i <=2; $i++) {
                    // Choose the subject for the current iteration
                    

                    SchoolGroup::create([
                        'student_id' => $student->student_id,
                        'subject_id' => $i+1,
                        'group_name' => 'b',
                        'score' => 0.00,
                    ]);
                }
            }
        }
    }
}
