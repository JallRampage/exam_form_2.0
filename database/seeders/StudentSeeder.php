<?php

namespace Database\Seeders;

use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StudentSeeder extends Seeder
{

    public function run(): void
    {
        $faker = Faker::create();
        $userId = 2;
        $controlNumberPrefix = 19;
        $semester = 1;

        for ($i = 0; $i < 30; $i++) {
            // Name and email generation
            $name = $faker->firstName . ' ' . $faker->lastName . ' ' . $faker->lastName;
            $email = strtolower(str_replace(' ', '.', $name)) . '@morelia.tecnm.mx';

            // Password generation
            $password = bcrypt('password');

            // Create User
            User::create([
                'name' => $name,
                'email' => $email,
                'password' => $password,
            ]);

            // Control number generation
            $controlNumber = $controlNumberPrefix . str_pad(($i % 20) + 1, 6, '0', STR_PAD_LEFT);

            // Create Student
            Student::create([
                'user_id' => $userId,
                'control_number' => $controlNumber,
                'semester' => $semester,
            ]);

            // Update variables for next iteration
            $userId++;

            if (($i + 1) % 20 == 0) {
                $controlNumberPrefix++;
                $semester++;
            }
        }
    }
}
