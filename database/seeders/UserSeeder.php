<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       User::create([
            'name' => 'Alan',
            'email' => 'Alan@g.com',
            'password' => bcrypt('12345678')
        ])->assignRole('Admin');
       User::create([
            'name' => 'Ala',
            'email' => 'Al@g.com',
            'password' => bcrypt('12345678')
        ]);



    }
}
