<x-modal wire:model='newexman_modal'>
    <div class="p-2 text-center font-bold">
        {{ $title }}
    </div>
    <br />
    <em class="ml-3">Creating new Exam::</em>
    <br />
    <form wire:submit.prevent='create_exam({{ $id_sub }})'>
    <label for="" class="ml-3 p-2 m-2">
        Exam Name :
        <input type="text" class="ml-3 p-2 m-2" wire:model="ex_name">
    </label>
    @for($i=1;$i<=10;$i++)
    <div class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 flex flex-col space-y-4" wire:key="{{$i}}">   
         <label for="" class="ml-3 p-2 m-2">
           {{$i}}.{{"-"}} <input type="text" class="ml-3 " wire:model="qm.{{$i}}.question_content" required>
         </label> 
         @for($j=1;$j<=4;$j++)
                <div class="flex flex-col space-y-2">  
                    <label class="inline-flex items-center">
                        <input type="radio" name="{{'radio'}}.{{$i}}" id="{{$i}}.{{$j}}" class="ml-3 " wire:click='comprobar({{$i}},{{$j}})' value="1" required>
                        <input type="text" class="ml-3 " wire:model="qm.{{$i}}.answers.{{$j}}.answer_content" required>
                    </label>
                </div>
        @endfor
            <br class="ml-3 p-2 m-2">
    </div>
    @endfor
    
    <button type="submit">Enviar</button>



     </form>
</x-modal>
