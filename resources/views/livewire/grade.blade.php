<div>
    <strong>Bienvenid@ {{Auth::User()->name}}</strong>
    @switch($aux3)
        @case(1)
        <p>Selecciona la materia :</p>
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th class="p-2">N.o</th>
                    <th class="p-2">Subject_name</th>
                    <th class="p-2">Score</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($school_group_info as $index =>$i)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td class="p-2">{{ $index+1}}</td>
                        <td class="p-2">{{ $i->subject->subject_name }}</td>
                        <td class="p-2">{{ $i->score }}</td>
                        <td ><button wire:click="gotoexams({{ $i->subject_id }})">View details</button>
                        </tr>
                @endforeach
            </tbody>
        </table>    
            @break
        @case(2)
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th class="p-2">N.o</th>
                    <th class="p-2">Exam_name</th>
                    <th class="p-2">Grade</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($exams as $index =>$i)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td class="p-2">{{ $index+1}}</td>
                        <td class="p-2">{{ $i->exam->exam_name}}</td>
                        <td class="p-2">{{ $i->grade }}</td>
                        </tr>
                @endforeach
            </tbody>
            @break
        @default
            
    @endswitch
    
</div>
