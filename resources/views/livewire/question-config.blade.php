<div>
    @if(session()->has('message'))
    <div class="bg-green-500 text-white font-bold rounded-t px-4 py-2">
        {{ session()->get('message') }}
    </div>
    @endif
    <strong>{{ $exam_name }} Exam Personalizing</strong>
    <h3>Question and Answers</h3>
    <form wire:submit.prevent='guardarCambios'>
    @foreach ($questions as $index => $j)
        <div class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 flex flex-col space-y-4">   
         <label for="">
            {{$index+1}}.{{'-'}}
            <input type="text"size="80" wire:model="questions_modificadas.{{ $index }}.question_content">
         </label> 
            @foreach ($j->answers as $answer_index => $answer)
                <div class="flex flex-col space-y-2">  
                    <label class="inline-flex items-center">
                        <input type="radio" name="correct_answer{{ $j->question_id }}" value="{{(string)'1'}}"
                        wire:model.defer="questions_modificadas.{{ $index }}.answers.{{ $answer_index }}.is_correct"      {{ $answer->is_correct == 1 ? 'checked' : '' }}>
                        <input type="text" wire:model="questions_modificadas.{{ $index }}.answers.{{ $answer_index }}.answer_content">
                    </label>
                </div>
            @endforeach
                <br>
        </div>
    @endforeach
    <button type="submit" class="border-1 rounded p-2 bg-gray-300" >Guardar cambios</button>



    </form>

</div>



