<div>
    @switch($aux)
        @case(1)
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th class="p-2">id</th>
                        <th class="p-2">Subjects</th>
                        <th class="p-2" colspan="2">Options</th>
                    </tr>
                 </thead>
                <tbody>
                    @foreach ($SchoolGroups as $u)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="p-2">{{ $u->subject_id}}</td>
                            <td class="p-2">{{ $u->subject_name}}</td>
                            <td ><button wire:click="gotoexams({{ $u->subject_id}})">View Exams</button></td>
                        </tr>
                    @endforeach
                </tbody>
        
                </table>
                <br>
                <div class="w-full p-3 m-3">
                    {{ $SchoolGroups->links() }}
                </div>
        @break
        @case(2)
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th class="p-2">id_exam</th>
                        <th class="p-2">Exam_name</th>
                        <th class="p-2">Status</th>
                        <th class="p-2 text-center">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($exams as $i)
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <td class="p-2">{{ $i->exam_id }}</td>
                            <td class="p-2">{{ $i->exam_name }}</td>
                            <td class="p-2">{{ $i->status == 1 ? 'assigned' : 'unassigned' }}</td>
                            <td class="flex items-center justify-center">
                                <button wire:click="gotoquestions({{ $i->exam_id }})" class="mx-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded">Exam config</button>
                                <button wire:click="changestatus({{ $i->exam_id }})" class="mx-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded">Assign / Unassign Exam</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <button class="p-2 m-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded" wire:click="prepare">New Exam</button>
            @include('livewire.exam_modal')
        @break
        @case(3)
        <livewire:question-config :busqueda="$busqueda" :exam_id="$id_ex" />
        

        @break

        @default
                
        @endswitch


</div>
