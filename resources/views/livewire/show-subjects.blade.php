<div>
    @switch($aux2)
        @case(1)
        @if(session()->has('message'))
            <div class="bg-green-500 text-white font-bold rounded-t px-4 py-2">
                {{ session()->get('message') }}
                <button type="submit"  class="border-1 rounded p-2 bg-green-500 text-white font-bold rounded-t px-4 py-2"><a href="{{route('subjects_student')}}">Aceptar</a></button>            
            </div>
        @endif
            <strong>Bienvenid@ {{Auth::User()->name}}</strong>
            <br>
            <label for="">Selecciona una materia</label>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th class="p-2">N.o</th>
                                <th class="p-2">Subject_name</th>
                                <th class="p-2">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($subject as $index =>$i)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td class="p-2">{{ $index}}</td>
                                    <td class="p-2">{{ $i->subject_name }}</td>
                                    <td ><button wire:click="gotoexams({{ $i->subject_id }}, '{{ $id_group_collect[$index] }}')">Go to exams</button>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
            @break
            @case(2)
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs uppercase  text-black dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th class="p-2">N.O</th>
                        <th class="p-2">Exam_name</th>
                        <th class="p-2">Status</th>
                        <th class="p-2 text-center">Options</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($exams as $index => $i)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <td class="p-2">{{ $index+1 }}</td>
                        <td class="p-2">{{ $i->exam_name }}</td>
                        @if (isset($evaluation[$aux_cont]) && $evaluation[$aux_cont]->exam_id == $i->exam_id)
                            <td class="p-2">{{ $evaluation[$aux_cont]->answered == 1 ? 'Already Answered' : ($i->status == 1 ? 'available' : 'Not available') }}</td>
                            <td class="flex items-center justify-center">
                                <button wire:click="gototest({{ $i->exam_id }})" class="mx-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded {{  $evaluation[$aux_cont]->answered == 1 ? 'opacity-50 cursor-not-allowed' :($i->status == 1 ? '' : 'opacity-50 cursor-not-allowed') }} "{{$evaluation[$aux_cont]->answered == 1 ?'disabled':($i->status == 1 ? '' :'disabled')}}>Start Exam</button>
                            </td>
                            @php
                                $aux_cont--;
                            @endphp
                            
                        @else
                            <td class="p-2">{{ $i->status == 1 ? 'available' : 'Not available' }}</td>
                            <td class="flex items-center justify-center">
                                <button wire:click="gototest({{ $i->exam_id }})" class="mx-1 bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 rounded {{ $i->status == 1 ? '' : 'opacity-50 cursor-not-allowed' }} "{{$i->status==1?'':'disabled'}}>Start Exam</button>
                            </td>
                        @endif
                    </tr>
                @endforeach
                
                </tbody>
            </table>
            @break
            @case(3)
                <div>
                    <strong>{{ $exam_name->exam_name }} Exam</strong>
                    <h3>Question and Answers</h3>
                    <form wire:submit.prevent='calificar'>
                    @foreach ($questions as $index => $j)
                        <div class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 flex flex-col space-y-4">   
                        <label for="">
                            {{$index+1}}.{{'-'}}
                            <label for="" wire:model="questions.{{ $index }}.question_content">{{$j->question_content}}</label>
                        </label> 
                            @foreach ($j->answers as $answer_index => $answer)
                                <div class="flex flex-col space-y-2">  
                                    <label class="inline-flex items-center">
                                        <input type="radio" name="correct_answer{{ $j->question_id }}" value="1" 
                                        wire:model.defer="answers_student.{{$index}}.answer.{{$answer_index}}.is_correct" >
                                        <label for="">{{' '.$answer->answer_content}}</label>
                                    </label>
                                </div>
                            @endforeach
                                <br>
                        </div>
                    @endforeach
                    <button type="submit" class="border-1 rounded p-2 bg-gray-300" >Enviar</button>



                    </form>

                </div>
            @break
            @default
                
            @endswitch
</div>
