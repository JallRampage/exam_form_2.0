<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\showschoolgroupController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboardAdmin', function () {
            return view('DashboardAdmin');
    })->middleware('can:dashboard')->name('dashboardAdmin');
    
    Route::get('/dashboard', function () {
            return view('dashboard');
        
    })->name('dashboard');
    
});
Route::get('/schoolgroups', [showschoolgroupController::class, 'index'])->middleware(['auth', 'verified'])->name('schoolgroups');
Route::get('/subjects', [showschoolgroupController::class, 'aux'])->middleware(['auth', 'verified'])->name('subjects_student');
Route::get('/grades', [showschoolgroupController::class, 'grade'])->middleware(['auth', 'verified'])->name('grades');


